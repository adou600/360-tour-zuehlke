﻿using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public static class Scenes {


	public const string video_id_key = "VIDEO_ID_KEY";

	private static Dictionary<string, string> parameters = new Dictionary<string, string>();

	public static void Load(string sceneName, Dictionary<string, string> parameters = null) {
		Scenes.parameters = parameters;
		SceneManager.LoadScene(sceneName);
	}

	public static void Load(string sceneName, string paramKey, string paramValue) {
		Scenes.parameters.Add(paramKey, paramValue);
		SceneManager.LoadScene(sceneName);
	}

	public static Dictionary<string, string> getSceneParameters() {
		return parameters;
	}

	public static string getParam(string paramKey) {
		if (Scenes.parameters == null) return "";
		return parameters[paramKey];
	}

	public static void setParam(string paramKey, string paramValue) {
		Scenes.parameters.Add(paramKey, paramValue);
	}

}