﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

/// <summary>
/// Sources:
/// 	https://unity3d.com/fr/learn/tutorials/topics/graphics/videoplayer-component
/// 	https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html
/// 	https://creativechris.me/tag/video-preparecompleted/
/// 	https://stackoverflow.com/questions/41144054/using-new-unity-videoplayer-and-videoclip-api-to-play-video
/// </summary>
public class TextureChanger : MonoBehaviour {
	
	private GameObject cube;
	private GameObject sphere;
	private VideoPlayer videoPlayer;
	private System.Random random = new System.Random();

	private List<VideoClip> clips;

	private int currentTextureId = 0;

	private List<String> allVideoTextures = new List<String> ( new String[] {	
		"breakfast_room",
		"machine_learning",
		"main_table",
		"vr_blockchain_scrable"
	} );

	void Awake()
	{
		var videoToPlay = Scenes.getParam (Scenes.video_id_key);
		Debug.Log ("Awake for video " + videoToPlay);

		StartCoroutine (InitVideoPlayer(videoToPlay));
	}

	void OnDisable()
	{
		Debug.Log ("OnDisable");
		videoPlayer.prepareCompleted -= Prepared;
	}

	public void SetNextTextureToSphere(string videoToPlay = null) 
	{
		Debug.Log ("SetNextTextureToSphere");


		VideoClip runtimeVideoClip;
		if (videoToPlay == null || videoToPlay.Equals("")) {
			runtimeVideoClip = clips [GetNextTextureId ()];
		} else {
			runtimeVideoClip = clips.Find(x => x.name.Equals(videoToPlay));
			currentTextureId = clips.IndexOf (runtimeVideoClip);
		}

		videoPlayer.Stop ();
		videoPlayer.clip = runtimeVideoClip;
		videoPlayer.Play ();
		Debug.Log ("video player will be prepared with video " + runtimeVideoClip.name);
		videoPlayer.Prepare ();

		MoveAndScaleCube ();
	}

	private IEnumerator InitVideoPlayer(string videoToPlay)
	{
		Debug.Log ("InitVideoPlayer for video " + videoToPlay);

		sphere = GameObject.Find ("Sphere 1");
		cube = GameObject.Find("Cube");

		//Add VideoPlayer to the sphere
		videoPlayer = sphere.AddComponent<VideoPlayer>();
		videoPlayer.playOnAwake = false;
		videoPlayer.isLooping = true;
		videoPlayer.source = VideoSource.VideoClip;
		videoPlayer.prepareCompleted += Prepared;

		//Load clips resources
		clips = LoadClipsResources ();

		//Init video textures
		SetNextTextureToSphere (videoToPlay);

		yield return null;
	}

	private List<VideoClip> LoadClipsResources()
	{
		List<VideoClip> clips = new List<VideoClip>();
		foreach (String clipName in allVideoTextures) {
			clips.Add (Resources.Load (this.allVideoTextures [GetNextTextureId ()]) as VideoClip);
		}

		return clips;
	}

	private void MoveAndScaleCube()
	{
		cube.transform.position = new Vector3 (GetRandomPosition(), GetRandomPosition(), GetRandomPosition());
		cube.transform.rotation = new Quaternion (GetRandomPosition (), GetRandomPosition (), GetRandomPosition (), 0);
		float newScale = GetRandomScale ();
		cube.transform.localScale = new Vector3 (newScale, newScale, newScale);
	}

	void Prepared(UnityEngine.Video.VideoPlayer vPlayer) 
	{
		Debug.Log ("Prepared, will start video " + vPlayer.clip.name);
		vPlayer.Play();
	}

	private float GetRandomPosition()
	{   
		float randomNumber = ((float) random.NextDouble () * 1.5f) + 0.5f;
		return random.NextDouble() > 0.5f ? randomNumber : randomNumber * -1;
	}

	private float GetRandomScale()
	{   
		return (float) (random.NextDouble() * (0.2 - 0.1) + 0.05);
	}

	private int GetNextTextureId() 
	{
		currentTextureId = (currentTextureId + 1) % this.allVideoTextures.Count;
		return currentTextureId;
	}

}
