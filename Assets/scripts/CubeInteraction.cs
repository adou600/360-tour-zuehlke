﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// See https://www.youtube.com/watch?v=iW0mP-hpRkk
/// </summary>
public class CubeInteraction : MonoBehaviour {
	

	public float gazeTime = 2f;
	private float timer;
	private bool gazedAt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var test = GetComponent<Renderer> ().bounds.size;
		if (gazedAt) {
			timer += Time.deltaTime;

			Transform child = transform.GetChild (0);
			Vector3 newScale = new Vector3 (timer / gazeTime, child.localScale.y, child.localScale.z);
			Vector3 newPosition = new Vector3 ((timer / gazeTime) / 2 - 0.5f, child.localPosition.y, child.localPosition.z);
			child.localScale = newScale;
			child.localPosition = newPosition;

			if (timer >= gazeTime) {
				ExecuteEvents.Execute (gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerDownHandler);
				GetComponent<Collider> ().enabled = false;
				timer = 0f;
			}
		}
	}

	public void PointerEnter () {
		Debug.Log ("PointerEnter");
		gazedAt = true;
	}

	public void PointerExit () {
		Debug.Log ("PointerExit");
		gazedAt = false;
		timer = 0f;

		Transform child = transform.GetChild (0);
		Vector3 newScale = new Vector3 (0f, child.localScale.y, child.localScale.z);
		Vector3 newPosition = new Vector3 (-0.5f, child.localPosition.y, child.localPosition.z);

		child.localScale = newScale;
		child.localPosition = newPosition;
	}

	public void PointerDown () {
		
		Debug.Log ("PointerDown");

		Scenes.Load ("360-tour", Scenes.video_id_key, gameObject.tag);
	}
}
