## Description

Project done during the Zühlke BCS Team Camp 2017 in Riederalp.

### Done

 - initial commit after tutorial https://youtu.be/2q9wc9Y35wg
 - change texture of the sphere programatically
 - remove the second sphere because not needed anymore
 - randomly place the cube inside the sphere 
 - use videos instead of images and get rid of sphere changer
 - add menu to select video to play
 - add video preview to the menu
 - add terrain to the menu


### Todo

 - use Youtube videos instead of local files
    - see unity asset in store: https://assetstore.unity.com/packages/tools/video/youtube-video-player-youtube-api-v3-29704) 
    - see Youtube Channel: https://www.youtube.com/playlist?list=PLRYpmTgh1b62IpaeyO0FMeuWo6Rnj-zko
 - integrate live streams from Youtube, with 360 degrees videos
 
### References

 - https://www.youtube.com/watch?v=txx_WSxLL-4
 - https://www.youtube.com/watch?v=8a3oWpyWJw4
 - https://www.youtube.com/watch?v=2q9wc9Y35wg
 - https://www.youtube.com/watch?v=iW0mP-hpRkk
 